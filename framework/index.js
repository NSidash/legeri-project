import { Registration, EmailValidation } from './services/index';



const apiProvider = () => ({
  registration: () => new Registration(),
  EmailValidation: () => new EmailValidation(),
});

export { apiProvider };

