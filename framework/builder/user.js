import faker from 'faker';

function deepCopy(example) {
  return JSON.parse(JSON.stringify(example));
}
const BuildUser = function () {
  // eslint-disable-next-line consistent-return
  this.get = function (type) {
    // eslint-disable-next-line default-case
    switch (type) {
      case 'new':
        // eslint-disable-next-line no-case-declarations
        const data = {
          login: faker.internet.email(),
          password: faker.vehicle.vin(),
          birth_date: '2020-12-07T17:09:14.344Z',
          nick: 'string22',
        };
        return deepCopy(data);
    }
  };
};

export { BuildUser };
