import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../lib/decorate';

const Registration = function Registration() {
  this.post = async function post(params) {
    const r = await supertest(urls.demo).post('/api/v1/register').send(params);
    return r;
  };
};

decorateService(Registration);

export { Registration };

const EmailValidation = function EmailValidation() {
  this.get = async function get(access_key,email) {
    const r = await supertest(urls.mailboxer).get(`/api/check?access_key=${access_key}&email=${email}`);
    return r;
  };
};

export { EmailValidation };
