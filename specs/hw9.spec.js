import { apiProvider } from '../framework';



test('Проверка на валидность', async () => {
    const r = await apiProvider().EmailValidation().get('e9cc4acb93964a4b643e746fb36876179, legeri@rambler.ru');
    expect(r.status).toBe(200);

});

test.each`
  email                   | access_key                             | expected 
  ${'legeri'}             | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}
  ${'legeri@'}            | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}
  ${'legeri@rambler'}     | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}
  ${'legeri@ramblerru'}   | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}
  ${'null'}               | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}  
  ${'123'}                | ${'9cc4acb93964a4b643e746fb36876179'}  | ${false}
  
  `('Передать невалидный адрес почты: $email', async ({email, access_key, expected}) => {
    const r = await apiProvider().EmailValidation().get(access_key, email);
    expect(r.body.format_valid).toBe(expected); 
});
  
test('Проверка почты без api key', async () => {
    const r = await apiProvider().EmailValidation().get(null, 'legeri@rambler.ru');
    expect(r.body.error.type).toBe("invalid_access_key");

});
// rrrrrr