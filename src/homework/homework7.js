/* Напишите функцию и набор тестов на неё.
Добавьте в комментариях, как работает ваша функция.
 Что должна делать функция: 1. Функция на вход принимает объект, в котором хранятся оценки за ДЗ нашей группы.
 Например, объект:
 const scores = {
 Anna: 10,
 Olga: 1,
 Ivan: 5,
}
 2. Функция возвращает в ответ сумму всех баллов */



export function JournalMarks() {

  let Marks = {
    Ben: 1,
    Sam: 2,
    Dave: 3,
    June: 4,
    Linda: 5

  };

  let Sum = 0

  for (let key in Marks) {
    if (Marks[key] === null || Marks[key] === undefined || typeof Marks[key] !== 'number' || Marks[key] === 0 || Marks[key] > 5)
      {throw new Error ("Невозможно распознать оценку")}

   else 
    Sum += Marks[key]
  }

  return (Sum)
};





