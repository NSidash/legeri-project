/**
 * kolobok
 * @param {string} name
 * @returns {string}
 */
export const kolobok = name => {
    switch (name.toLowerCase()) {

        case 'дедушка':
            return 'Я от дедушки ушёл';
         case 'бабушка':
              return 'Я от бабушки ушел';
         case 'медведь':
         case 'заяц':
         case 'волк':
             return 'И от тебя тоже уйду';
         case 'лиса':
             return 'Что-то плохо слышно твою песню, Колобочек, сядь мне на носик...';
         default: 
             return 'Сказкой ошиблись';
 
      }
  };
 
/**
 * kolobok
 * @param {string} name
 * @returns {string}
 */

 export const NY = name => {
    if (name === 'Дед Мороз' || name === 'Снегурочка')
    return `${name}! ${name}! ${name}!`;
    if (name !== 'Дед Мороз' || name !== 'Снегурочка')
    return ('Ошибка. Неверное или неточное имя.');
 };
